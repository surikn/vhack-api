from django.contrib import admin
from .models import Contact, Test, TestQuestion, TestQuestion, TestQuestionChoice, TestResults

admin.site.register(Contact)
admin.site.register(Test)
admin.site.register(TestQuestion)
admin.site.register(TestQuestionChoice)
admin.site.register(TestResults)
