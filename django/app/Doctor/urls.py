from django.urls import path
from django.conf.urls import include
from . import views

urlpatterns = [
    path("login", views.login_page),
    path("logout", views.logout),
    path("cabinet", views.cabinet),

    path("client/<str:login>", views.client_page),

    path("auth", views.auth),
]
