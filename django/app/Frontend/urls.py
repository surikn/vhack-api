from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from . import views

urlpatterns = [
    path('', views.landpage),
    path('api/', include("API.urls")),
    path('doctor/', include('Doctor.urls')),
   
    path('client/', include('Client.urls')),
    path('admin/', admin.site.urls),
]
