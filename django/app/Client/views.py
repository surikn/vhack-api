from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .models import Client, Request, PainRegion, ChatMessage
from django.views.decorators.csrf import csrf_exempt
from django.core.files.storage import FileSystemStorage
from django.conf import settings
from django.core import serializers
from datetime import datetime
import json
import os


@csrf_exempt
def auth(request):
    body_unicode = request.body.decode('utf-8')
    body = json.loads(body_unicode)

    if "login" not in body or "password" not in body:
        return HttpResponse(json.dumps({"ok": False, "error": "Invalid request"}), content_type="application/json")
    
    login = body["login"]
    password = body["password"]

    client_q = Client.objects.filter(login=login, password=password)
    if client_q.count() == 0:
        return HttpResponse(json.dumps({"ok": False, "error": "Неправильный логин или пароль"}), content_type="application/json")
    
    return HttpResponse(json.dumps({"ok": True, "name": client_q[0].name}), content_type="application/json")


@csrf_exempt
def register(request):
    body_unicode = request.body.decode('utf-8')
    body = json.loads(body_unicode)

    login = body["login"]
    password = body["password"]
    name = body["name"]

    l_cl = Client.objects.filter(login=login)
    if l_cl.count() != 0:
        return HttpResponse(json.dumps({"ok": False, "error": "Такой логин уже существует"}), content_type="application/json")

    Client.objects.create(login=login, password=password, name=name, age=60, sex="F", diagnosis="-", additional_information="-")
    return HttpResponse(json.dumps({"ok": True}), content_type="application/json")


@csrf_exempt
def update_client(request):
    try:
        login = request.POST["login"]
        password = request.POST["password"]   
 
        name = request.POST["name"]
        sex = request.POST["sex"]
        age = request.POST["age"]

        diag = request.POST["diag"]
        add_info = request.POST["add_info"]
    except Exception as ex:
        return HttpResponse(json.dumps({"ok": False, "error": "Invalid request"}), content_type="application/json")

    client = Client.objects.filter(login=login, password=password)
    if client.count() == 0:
        return HttpResponse(json.dumps({"ok": False, "error": "Неправильный логин или пароль"}), content_type="application/json")

    client = client[0]
    client.name = name
    client.sex = sex
    client.age = int(age)
    client.diagnosis = diag
    client.additional_information = add_info
    client.save()

    return HttpResponse(json.dumps({"ok": True}), content_type="application/json")


@csrf_exempt
def send_request(request):
    try:
        login = request.POST["login"]
        title = request.POST["title"]
        description = request.POST["description"]
        acc = request.POST["access"]
    except Exception as ex:
        return HttpResponse(json.dumps({"ok": False, "error": "Invalid request"}), content_type="application/json")

    Request.objects.create(title=title, description=description, client_login=login, client_data_access=int(acc))
    return HttpResponse(json.dumps({"ok": True}), content_type="application/json")


@csrf_exempt
def add_pain_region(request):
#   print("****** POST ********")
#   for key, value in request.POST.items():
#      print('Key: %s' % (key) ) 
#        # print(f'Key: {key}') in Python >= 3.7
#        print('Value %s' % (value) )

#    print("******* FILES ********")
#    for key, value in request.FILES.items():
#        print('Key: %s' % (key) ) 
#        # print(f'Key: {key}') in Python >= 3.7
#        print('Value %s' % (value) )
    if "point" not in request.POST or "image" not in request.FILES:
        return HttpResponse(json.dumps({"ok": False, "error": "Invalid request"}), content_type="application/json")
    
    post_data = json.loads(request.POST["point"])

    x = float(post_data["x"])
    y = float(post_data["y"])
    r = float(post_data["r"])
    comment = post_data["comment"]
    image = request.FILES["image"]
    login = post_data["login"]

    fs = FileSystemStorage()
    image_path = os.path.join(settings.BASE_DIR, "static", "pain_images", image.name)
    image_path = fs.save(image_path, image)
    image_path = "http://" + settings.SERVER_NAME + "/" + "/".join(image_path.split("/")[2:])    

    pr = PainRegion.objects.create(client_login=login, x=x, y=y, r=r, comment=comment, image_path=image_path)
    return HttpResponse(json.dumps({"ok": True, "pr_id": pr.id, "image_name": image_path.split("/")[-1]}), content_type="application/json")


@csrf_exempt
def get_pain_regions(request):
    if "login" not in request.GET or "date" not in request.GET:
        return HttpResponse(json.dumps({"ok": False, "error": "Invalid request"}), content_type="application/json")

    login = request.GET["login"]
    date = request.GET["date"]

    date = datetime.date(datetime.strptime(date, "%Y-%m-%d"))
    pain_regions = PainRegion.objects.filter(client_login=login, add_date__year=date.year, add_date__month=date.month, add_date__day=date.day)

    pain_regions_json = serializers.serialize("json", pain_regions)
    response = {"ok": True, "regions": [contact["fields"] for contact in json.loads(pain_regions_json)]}
    return HttpResponse(json.dumps(response), content_type="application/json")


@csrf_exempt
def get_chat_messages(request):
    messages = ChatMessage.objects.all().order_by("-date")

    response = {"ok": True, "messages": []}
    for m in messages:
        try:
            login = m.client_login
            client = Client.objects.get(login=login)
            response["messages"].append({"name": client.name, "login": login, "text": m.text})
        except Exception as ex:
            pass

    return HttpResponse(json.dumps(response), content_type="application/json")


@csrf_exempt
def send_chat_message(request):
    login = request.GET["login"]
    text = request.GET["text"]

    ChatMessage.objects.create(client_login=login, text=text)
    return HttpResponse(json.dumps({"ok": True}), content_type="application/json")

