from django.db import models
from django.conf import settings
import json
import os


class Contact(models.Model):
    name = models.CharField(max_length=64)
    description = models.CharField(max_length=64)
    icon = models.TextField()


class TestQuestionChoice(models.Model):
    question_id = models.IntegerField()

    text = models.TextField()
    score = models.IntegerField()

class TestResults(models.Model):
    test_id = models.IntegerField()

    range_start = models.IntegerField()
    range_end = models.IntegerField()

    result_text = models.TextField()
    result_image = models.TextField()
    

class TestQuestion(models.Model):
    test_id = models.IntegerField()
    text = models.TextField()
    type = models.CharField(max_length=32)


class Test(models.Model):
    name = models.CharField(max_length=32)
    description = models.TextField()
    icon = models.TextField()

