from django.shortcuts import render
from django.http import HttpResponse
from django.core import serializers
from .models import Contact, Test, TestQuestionChoice, TestQuestion, TestResults
from .create_db import create_contacts, create_tests1, create_tests2, create_clients, create_doctors, create_pain_regions
from django.views.decorators.csrf import csrf_exempt
import json

@csrf_exempt
def fill_db(request):
    create_contacts()
    create_tests1()
    create_tests2()

    create_clients()
    create_doctors()
    create_pain_regions()
    return HttpResponse("OK")


@csrf_exempt
def contacts(request):
    contacts = Contact.objects.all()
    contacts_json = serializers.serialize("json", contacts)
    response = {'contacts': [contact["fields"] for contact in json.loads(contacts_json)]}
    return HttpResponse(json.dumps(response), content_type="application/json")


@csrf_exempt
def get_test_list(request):
    tests = Test.objects.all()
    tests_json = serializers.serialize("json", tests)
    response = []
    for test in json.loads(tests_json):
        new_item = test["fields"]
        new_item["id"] = test["pk"]
        response.append(new_item)
    response = {'tests': response}
    return HttpResponse(json.dumps(response), content_type="application/json")


@csrf_exempt
def get_test(request, test_id):
    test = Test.objects.filter(id=test_id)
    if test.count() == 0:
        return HttpResponse(json.dumps({"ok": False, "error": "404"}))
    response = {}
    test = test[0]

    response["form_id"] = test.id
    response["form_name"] = test.name
    response["questions"] = []

    questions = TestQuestion.objects.filter(test_id=test.id)
    for q in questions:
        choices_q = TestQuestionChoice.objects.filter(question_id=q.id)
        choices_text = [ch.text for ch in choices_q]
        choices_scores = [ch.score for ch in choices_q]
        response["questions"].append({
            "question_id": q.id,
            "question_text": q.text,
            "type": "radio",
            "choices": choices_text,
            "scores": choices_scores,
        })

    response["ok"] = True
    return HttpResponse(json.dumps(response), content_type="application/json")

@csrf_exempt
def get_test_result(request):
    test_id = request.GET["test_id"]
    score = request.GET["score"]
    response = {}

    results = TestResults.objects.filter(test_id=test_id)
    for r in results:
        if r.range_start <= int(score) <= r.range_end:
            response['text'] = r.result_text
            response['image'] = r.result_image
            break
    
    return HttpResponse(json.dumps(response), content_type="application/json")
