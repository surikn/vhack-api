from django.contrib import admin
from .models import Client, Request, PainRegion, ChatMessage

admin.site.register(Client)
admin.site.register(Request)
admin.site.register(PainRegion)
admin.site.register(ChatMessage)
