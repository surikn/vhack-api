from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from Client.models import Client, PainRegion
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from .models import Doctor
import pandas as pd
import json

def cabinet(request):
    if "id" not in request.session:
        return HttpResponseRedirect("/doctor/login")
    clients = Client.objects.all()
    return render(request, "Cabinet/index.html", {"clients": clients})


def login_page(request):
    if "id" in request.session:
        return HttpResponseRedirect("/doctor/cabinet")
    return render(request, "Auth/index.html")


def client_page(request, login):
    if "id" not in request.session:
        return HttpResponseRedirect("/doctor/login")   
    try:
        client = Client.objects.get(login=login)
    except Exception as ex:
        print("ERROR: ", ex)
        return HttpResponse("404")

    pain_regions = PainRegion.objects.filter(client_login=login).order_by("add_date")

    pain_regions_json = serializers.serialize("json", pain_regions)
    pain_regions_data = [pr["fields"] for pr in json.loads(pain_regions_json)]

    pain_data = pd.DataFrame()    
    for i, p in enumerate(pain_regions_data):
        n = pd.DataFrame(p, index=[i])
        pain_data = pd.concat((pain_data, n))

    pain_data["add_date"] = pd.to_datetime(pain_data["add_date"]).dt.date
    sum_pain = pain_data.groupby("add_date")["r"].sum()
    sum_pain = sum_pain * 100

    pain_plot_data_date = sum_pain.index.tolist()
    pain_plot_data_val = sum_pain.values.tolist()
    pain_plot_data_date = list(map(lambda x: pd.to_datetime(str(x)).strftime('%Y-%m-%d'), sum_pain.index))

    return render(request, "Client/index.html", {"client": client, "pain_regions": pain_regions, "pain_plot_date": pain_plot_data_date, "pain_plot_val": pain_plot_data_val})


def logout(request):
    if "id" in request.session:
        del request.session["id"]
    return HttpResponseRedirect("/doctor/login")


@csrf_exempt
def auth(request):
    if request.method == "POST":
        if "login" not in request.POST or "password" not in request.POST:
            return HttpResponse(json.dumps({"ok": False, "error": "Invalid reqest"}), content_type="application/json")
        login = request.POST["login"]
        password = request.POST["password"]
        
        if login == "" or password == "":
            return HttpResponse(json.dumps({"ok": False, "error": "Заполните все поля"}), content_type="application/json") 

        doctor_q = Doctor.objects.filter(login=login, password=password)
        if doctor_q.count() == 0:
            return HttpResponse(json.dumps({"ok": False, "error": "Неправильный логин или пароль"}), content_type="application/json") 
        request.session["id"] = doctor_q[0].id
        return HttpResponse(json.dumps({"ok": True}), content_type="application/json")

    return HttpResponse(json.dumps({"ok": False, "error": "Invalid reqest"}), content_type="application/json")
