# это можно делать нормально на админском сайте, но так удобнее дебажить 
from .models import *
from Doctor.models import Doctor
from Client.models import Client, PainRegion
from datetime import datetime

def create_contacts():
    c1 = Contact(
        name="Горячая линия",
        description="Номер телефона: 8-800-700-84-36",
        icon="https://upload.wikimedia.org/wikipedia/ru/thumb/e/e2/Vera_black_dan_copy.jpg/375px-Vera_black_dan_copy.jpg"
    )
    c2 = Contact(
        name="Телефон доверия",
        description= "Номер телефона: 8-800-2000-122",
        icon="https://telefon-doveria.ru/wp-content/uploads/2016/08/logo.png"
    )
    c1.save()
    c2.save()


def create_doctors():
    Doctor.objects.create(login="doctor1", password="doctor1", name="Никита", surname="Сурначев")
    Doctor.objects.create(login="doctor2", password="doctor2", name="Валерий", surname="Валериевич")


def create_clients():
    Client.objects.create(login="user1", password="user1", name="Александр Петрович", age=60, sex="M", diagnosis="Деменция", additional_information="-")
    Client.objects.create(login="user2", password="user2", name="Мария Васильевна", age=75, sex="F", diagnosis="болезнь Паркинсона", additional_information="-")


def create_pain_regions():
    imp = "https://www.thenakedscientists.com/sites/default/files/styles/social-media-large/public/media/media/images/bruise.jpg"
    imp1 = "https://media.beam.usnews.com/b2/ae/65e41a85420d8500d83608f69187/160613-bruisedarm-stock.jpg"
    imp2 = "https://i0.wp.com/cdn-prod.medicalnewstoday.com/content/images/articles/317/317575/bruise-on-skin.jpg"
    imp3 = "https://cdn-prod.medicalnewstoday.com/content/images/hero/325/325525/325525_1100.jpg"
    PainRegion.objects.create(client_login="user1", x=0.3, y=0.2, r=1, comment="резкая боль, периодичная", image_path=imp, add_date=datetime.strptime("2020-04-24", "%Y-%m-%d"))   
    PainRegion.objects.create(client_login="user1", x=0.4, y=0.2, r=8, comment="резкая боль, периодичная", image_path=imp, add_date=datetime.strptime("2020-04-25", "%Y-%m-%d"))
    PainRegion.objects.create(client_login="user1", x=0.5, y=0.6, r=7, comment="начались отеки, чешется", image_path=imp1, add_date=datetime.strptime("2020-04-26", "%Y-%m-%d"))
    PainRegion.objects.create(client_login="user1", x=0.64, y=0.2, r=1, comment="начались отеки, чешется", image_path=imp1, add_date=datetime.strptime("2020-04-27", "%Y-%m-%d"))
    PainRegion.objects.create(client_login="user1", x=0.3, y=0.8, r=5, comment="начались отеки, чешется", image_path=imp2, add_date=datetime.strptime("2020-04-28", "%Y-%m-%d"))
    PainRegion.objects.create(client_login="user1", x=0.38, y=0.2, r=4, comment="боль уменьшилась", image_path=imp2, add_date=datetime.strptime("2020-04-28", "%Y-%m-%d"))
    PainRegion.objects.create(client_login="user1", x=0.3, y=0.5, r=9, comment="боль уменьшилась", image_path=imp3, add_date=datetime.strptime("2020-04-30", "%Y-%m-%d"))
    PainRegion.objects.create(client_login="user1", x=0.15, y=0.6, r=8, comment="практически не чувствует боли в этом регионе", image_path=imp3, add_date=datetime.strptime("2020-05-01", "%Y-%m-%d"))
    PainRegion.objects.create(client_login="user1", x=0.3, y=0.2, r=9, comment="практически не чувствует боли в этом регионе", image_path=imp3, add_date=datetime.strptime("2020-05-02", "%Y-%m-%d"))

    PainRegion.objects.create(client_login="user2", x=0.3, y=0.2, r=5, comment="резкая боль, периодичная", image_path=imp, add_date=datetime.strptime("2020-04-24", "%Y-%m-%d"))
    PainRegion.objects.create(client_login="user2", x=0.4, y=0.2, r=5, comment="резкая боль, периодичная", image_path=imp, add_date=datetime.strptime("2020-04-25", "%Y-%m-%d"))
    PainRegion.objects.create(client_login="user2", x=0.5, y=0.6, r=7, comment="резкая боль, периодичная", image_path=imp, add_date=datetime.strptime("2020-04-26", "%Y-%m-%d"))
    PainRegion.objects.create(client_login="user2", x=0.4, y=0.2, r=4, comment="описание4", image_path=imp, add_date=datetime.strptime("2020-04-27", "%Y-%m-%d"))
    PainRegion.objects.create(client_login="user2", x=0.3, y=0.8, r=4, comment="описание5", image_path=imp, add_date=datetime.strptime("2020-04-28", "%Y-%m-%d"))
    PainRegion.objects.create(client_login="user2", x=0.3, y=0.2, r=5, comment="описание6", image_path=imp, add_date=datetime.strptime("2020-04-29", "%Y-%m-%d"))
    PainRegion.objects.create(client_login="user2", x=0.5, y=0.5, r=5, comment="описание7", image_path=imp, add_date=datetime.strptime("2020-04-30", "%Y-%m-%d"))
    PainRegion.objects.create(client_login="user2", x=0.3, y=0.6, r=7, comment="описание8", image_path=imp, add_date=datetime.strptime("2020-05-01", "%Y-%m-%d"))
    PainRegion.objects.create(client_login="user2", x=0.5, y=0.2, r=5, comment="описание9", image_path=imp, add_date=datetime.strptime("2020-05-02", "%Y-%m-%d"))


def create_tests1():
    test = Test(name="Шкала Нортона", description="Шкала оценки опасности возникновения пролежней", icon="https://cdn3.iconfinder.com/data/icons/medical-healthcare-thick-colored-outline/33/medical-98-512.png")
    test.save()

    tq1 = TestQuestion(test_id=test.id, text="Общее состояние", type="radio")
    tq1.save()
    qc1 = TestQuestionChoice(question_id=tq1.id, text="Хорошее", score=4)
    qc2 = TestQuestionChoice(question_id=tq1.id, text="Удовлетворительное", score=3)
    qc3 = TestQuestionChoice(question_id=tq1.id, text="Тяжелое", score=2)
    qc4 = TestQuestionChoice(question_id=tq1.id, text="Крайне тяжелое", score=1)
    qc1.save(), qc2.save(), qc3.save(), qc4.save()

    tq2 = TestQuestion(test_id=test.id, text="Психологическое состояние", type="radio")
    tq2.save()
    qc1 = TestQuestionChoice(question_id=tq2.id, text="Настороженное", score=4)
    qc2 = TestQuestionChoice(question_id=tq2.id, text="Апатия", score=3)
    qc3 = TestQuestionChoice(question_id=tq2.id, text="Дезориентированность", score=2)
    qc4 = TestQuestionChoice(question_id=tq2.id, text="Загруженность", score=1)
    qc1.save(), qc2.save(), qc3.save(), qc4.save()

    tq3 = TestQuestion(test_id=test.id, text="Активность", type="radio")
    tq3.save()
    qc1 = TestQuestionChoice(question_id=tq3.id, text="Ходьба", score=4)
    qc2 = TestQuestionChoice(question_id=tq3.id, text="С посторонней помощью", score=3)
    qc3 = TestQuestionChoice(question_id=tq3.id, text="Сидение в коляске", score=2)
    qc4 = TestQuestionChoice(question_id=tq3.id, text="Лежание в постели", score=1)
    qc1.save(), qc2.save(), qc3.save(), qc4.save()

    tq4 = TestQuestion(test_id=test.id, text="Подвижность", type="radio")
    tq4.save()
    qc1 = TestQuestionChoice(question_id=tq4.id, text="Общая, хорошая", score=4)
    qc2 = TestQuestionChoice(question_id=tq4.id, text="Несколько ограничена", score=3)
    qc3 = TestQuestionChoice(question_id=tq4.id, text="Сильно ограничена", score=2)
    qc4 = TestQuestionChoice(question_id=tq4.id, text="Обездвиженность", score=1)
    qc1.save(), qc2.save(), qc3.save(), qc4.save()

    tq5 = TestQuestion(test_id=test.id, text="Контроль за функциями таза", type="radio")
    tq5.save()
    qc1 = TestQuestionChoice(question_id=tq5.id, text="Недержание отсутствует", score=4)
    qc2 = TestQuestionChoice(question_id=tq5.id, text="Незначительное недержание", score=3)
    qc3 = TestQuestionChoice(question_id=tq5.id, text="Только мочи", score=2)
    qc4 = TestQuestionChoice(question_id=tq5.id, text="Двойное недержание", score=1)
    qc1.save(), qc2.save(), qc3.save(), qc4.save()

    tr1 = TestResults(
        test_id=test.id,
        range_start=0,
        range_end=12,
        result_text="Вы находитесь в зоне высокого риска. Вам нужно систематически принимать меры, чтобы избежать образования пролежней. Ознакомиться с ними вы можете на картинке ниже:",
        result_image="http://35.193.85.8/static/test_images/l2.jpg"
    )

    tr2 = TestResults(
        test_id=test.id,
        range_start=12,
        range_end=14,
        result_text="Вы находитесь в зоне умеренного риска. Вам следует придерживаться профилактических мер. Ознакомиться с ними вы можте на картинке ниже:",
        result_image="http://35.193.85.8/static/test_images/l4.jpg"
    )
    tr3 = TestResults(
        test_id=test.id,
        range_start=14,
        range_end=25,
        result_text="У вас отсутствует риск образования пролежней!",
        result_image=""
    )
    tr1.save(), tr2.save(), tr3.save()

def create_tests2():
    test = Test(name="Шкала Морса", description="Шкала оценки риска падений Морса", icon="https://cdn3.iconfinder.com/data/icons/medical-healthcare-thick-colored-outline/33/medical-98-512.png")
    test.save()

    tq1 = TestQuestion(test_id=test.id, text="Было падение в анамнезе", type="radio")
    tq1.save()
    qc1 = TestQuestionChoice(question_id=tq1.id, text="Да", score=25)
    qc2 = TestQuestionChoice(question_id=tq1.id, text="Нет", score=0)
    qc1.save(), qc2.save()

    tq2 = TestQuestion(test_id=test.id, text="Есть сопутствующие заболевания", type="radio")
    tq2.save()
    qc1 = TestQuestionChoice(question_id=tq2.id, text="Да", score=15)
    qc2 = TestQuestionChoice(question_id=tq2.id, text="Нет", score=0)
    qc1.save(), qc2.save()

    tq3 = TestQuestion(test_id=test.id, text="Имеет вспомогательное средство для перемещения", type="radio")
    tq3.save()
    qc1 = TestQuestionChoice(question_id=tq3.id, text="Постельный режим", score=0)
    qc2 = TestQuestionChoice(question_id=tq3.id, text="Костыли/Палка/Ходунки", score=15)
    qc3 = TestQuestionChoice(question_id=tq3.id, text="Придерживается при перемещении за мебель", score=30)
    qc1.save(), qc2.save(), qc3.save()

    tq4 = TestQuestion(test_id=test.id, text="Функция ходьбы", type="radio")
    tq4.save()
    qc1 = TestQuestionChoice(question_id=tq4.id, text="Норма/Постельный режим/Обездвижен", score=0)
    qc2 = TestQuestionChoice(question_id=tq4.id, text="Слабая", score=10)
    qc3 = TestQuestionChoice(question_id=tq4.id, text="Нарушена", score=20)
    qc1.save(), qc2.save(), qc3.save()

    tq5 = TestQuestion(test_id=test.id, text="Оценка пациентом собственных возможностей и ограничений", type="radio")
    tq5.save()
    qc1 = TestQuestionChoice(question_id=tq5.id, text="Знает свои ограничения", score=0)
    qc2 = TestQuestionChoice(question_id=tq5.id, text="Переоценивает свои возможности/Забывает об ограничениях", score=15)
    qc1.save(), qc2.save()


    tr1 = TestResults(
        test_id=test.id,
        range_start=46,
        range_end=150,
        result_text="Вы находитесь в зоне высокого риска. Вам нужно систематически принимать меры, чтобы избежать травм от падений. Ознакомиться с ними вы можете на картинке ниже:",
        result_image="http://35.193.85.8/static/test_images/f4.jpg"
    )

    tr2 = TestResults(
        test_id=test.id,
        range_start=25,
        range_end=45,
        result_text="Вы находитесь в зоне среднего риска. Вам следует придерживаться профилактических мер. Ознакомиться с ними вы можте на картинке ниже:",
        result_image="http://35.193.85.8/static/test_images/f2.jpg"
    )
    tr3 = TestResults(
        test_id=test.id,
        range_start=0,
        range_end=20,
        result_text="Вы находитесь в зоне низкого риска. ",
        result_image="http://35.193.85.8/static/test_images/f1.jpg"
    )
    tr1.save(), tr2.save(), tr3.save()


if __name__ == "__main__":
    create_contacts()
    create_tests1()
    create_tests2()
