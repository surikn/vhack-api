from django.urls import path
from django.conf.urls import include
from . import views

urlpatterns = [
    path("update_client", views.update_client),
    path("send_request", views.send_request),

    path("add_pain_region", views.add_pain_region),
    path("get_pain_regions", views.get_pain_regions),

    path("get_chat_messages", views.get_chat_messages),
    path("send_chat_message", views.send_chat_message),

    path("auth", views.auth),
    path("register", views.register),
]
