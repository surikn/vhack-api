from django.urls import path
from django.conf.urls import include
from . import views

urlpatterns = [
    path("contacts", views.contacts),
    path("test_list", views.get_test_list),
    path("test/<int:test_id>", views.get_test),
    path("get_test_result", views.get_test_result),
    path("filldb", views.fill_db),
]
