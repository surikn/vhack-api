from django.db import models
from django.conf import settings
from django.utils import timezone
import json
import os


class Client(models.Model):
    name = models.CharField(max_length=64)
    age = models.IntegerField()
    sex = models.CharField(max_length=16)

    login = models.CharField(max_length=32)
    password = models.CharField(max_length=32)

    diagnosis = models.TextField()
    additional_information = models.TextField()



class Request(models.Model):
    title = models.CharField(max_length=64)
    description = models.TextField()

    client_login = models.CharField(max_length=32)
    client_data_access = models.IntegerField()


class PainRegion(models.Model):
    client_login = models.CharField(max_length=32)
    add_date = models.DateTimeField(editable=False)
    modified = models.DateTimeField()

    x = models.FloatField()
    y = models.FloatField()
    r = models.FloatField()


    image_path = models.TextField()
    comment = models.TextField()

    def save(self, *args, **kwargs):
        print(self.add_date)
        if not self.add_date:
            self.add_date = timezone.now()
        self.modified = timezone.now()
        return super(PainRegion, self).save(*args, **kwargs)


class ChatMessage(models.Model):
    client_login = models.CharField(max_length=32)
    date = models.DateTimeField(editable=False)
    
    text = models.TextField()
    
    def save(self, *args, **kwargs):
        if not self.id:
            self.date = timezone.now()
        return super(ChatMessage, self).save(*args, **kwargs)


