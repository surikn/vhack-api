# Generated by Django 2.2.4 on 2020-05-04 11:52

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Client', '0003_auto_20200504_1006'),
    ]

    operations = [
        migrations.AddField(
            model_name='painregion',
            name='comment',
            field=models.TextField(default=datetime.datetime(2020, 5, 4, 11, 52, 13, 432317)),
            preserve_default=False,
        ),
    ]
