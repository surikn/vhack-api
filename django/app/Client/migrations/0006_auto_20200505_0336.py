# Generated by Django 2.2.4 on 2020-05-05 03:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Client', '0005_chatmessage'),
    ]

    operations = [
        migrations.AlterField(
            model_name='painregion',
            name='image_path',
            field=models.TextField(),
        ),
    ]
